<?php

/**
 * Classe utilizada para manipular os hosts virtuais do XAMPP
 *
 * @author tiago
 */
class Vhost {

    private $config = "config";
    private $httpd;
    private $httpd_conf;
    private $hosts;
    private $hostsOriginal;
    private $jfile = "jhosts";
    private $jhosts;
    private $name_virtual_host;
    private $ip_virtual_host;
    private $localhost_dir;

    /**
     * 
     * @param boolean $readConfig | Verifica se o arquivo de configurações deve ser lido ao instanciar a classe;
     */
    public function __construct($readConfig = true) {

        if (!file_exists($this->jfile)) {
            file_put_contents($this->jfile, "");
        }

        $this->jhosts = json_decode(file_get_contents($this->jfile));

        if ($readConfig === true) {
            $this->readConfig();
        }
    }

    /**
     * Verifica se o arquivo de configurações do vhost existe
     * @return boolean
     */
    public function configExistis() {
        if (file_exists($this->config)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lista os dados do arquivo de configurações do vhost
     * @return string|null
     */
    public function configList() {
        if ($this->configExistis()) {
            $config = $this->getConfig();
            $configShow = array(
                "name_virtual_host" => "Name Virtual Host",
                "ip_virtual_host" => "IP Virtual Host",
                "dir_hosts_so" => "Diretório do hosts",
                "dir_httpd" => "Diretório do httpd-vhosts.conf",
                "localhost_dir" => "Diretório de acesso ao localhost",
            );
            unset($configShow['ip_virtual_host']);

            $table = "<table id='tbConfigList'>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th>Parametro</th>";
            $table .= "<th>Valor</th>";
            $table .= "</tr>";
            $table .= "</thead>";
            $table .= "<tbody>";
            foreach ($configShow as $key => $value) {
                $table .= "<tr>";
                $table .= "<td>{$value}</td>";
                if (array_key_exists($key, $config)) {
                    $table .= "<td>{$config->$key}</td>";
                } else {
                    $table .= "<td>N&AtildeO INFORMADO</td>";
                }
                $table .= "</tr>";
            }
            $table .= "</tbody>";
            $table .= "</table>";

            return $table;
        } else {
            return null;
        }
    }

    /**
     * Retorna as configurações do Virtual Host
     * 
     * @return type
     */
    public function getConfig() {
        if (!$this->configExistis()) {
            header("location: /vhost/config.php");
            exit;
        } else {
            return json_decode(file_get_contents($this->config));
        }
    }

    /**
     * Retorna os dados de um virtual host através do seu ID
     * @param int $id | Posição do virtual host no arquivo de hosts do vhost
     * @return object
     */
    public function getHostById($id) {

        if (!$this->jhostIdExistis($id)) {
            $_SESSION['msgs'][] = array(
                'msg' => "O virtual host informado não existe!",
                'type' => "error"
            );

            header("location: " . $_SERVER['HTTP_REFERER']);
            exit;
        } else {
            return $this->jhosts[$id];
        }
    }

    private function readConfig() {
        $config = $this->getConfig();

        $this->hosts = $config->hosts;
        $this->hostsOriginal = $config->hostsOriginal;
        $this->httpd = $config->httpd;
        $this->httpd_conf = $config->httpd_conf;
        $this->name_virtual_host = $config->name_virtual_host;
//        $this->ip_virtual_host = $config->ip_virtual_host;
        $this->ip_host_so = $config->ip_host_so;
        $this->localhost_dir = $config->localhost_dir;
    }

    /**
     * Salva as configurações do Virtual Host
     * 
     * @param type $params
     */
    public function setConfig($params) {
        $dir_hosts_so = preg_replace("/(\/)+$/", "", $params['dir_hosts_so']);
        $dir_httpd = preg_replace("/(\/)+$/", "", $params['dir_httpd']);
        $name_virtual_host = $params['name_virtual_host'];
//        $ip_virtual_host = $params['ip_virtual_host'];
        $localhost_dir = $params['localhost_dir'];
        $config = array();

        $config["dir_hosts_so"] = $this->hosts = $dir_hosts_so;
        $config["hosts"] = $this->hosts = $dir_hosts_so . "/hosts";
        $config["hostsOriginal"] = $this->hostsOriginal = $this->hosts . "_original";
        $config["dir_httpd"] = $this->httpd = $dir_httpd;
        $config["httpd"] = $this->httpd = $dir_httpd . "/httpd-vhosts";
        $config["httpd_conf"] = $this->httpd_conf = $this->httpd . ".conf";
        $config["name_virtual_host"] = $this->name_virtual_host = $name_virtual_host;
//        $config["ip_virtual_host"] = $this->ip_virtual_host = $ip_virtual_host;
        $config["localhost_dir"] = $this->localhost_dir = $localhost_dir;

        unlink($this->config);
        file_put_contents($this->config, json_encode($config));
    }

    /**
     * Retorna o conteudo atual do arquivo de hosts da maquina
     * @return string
     */
    public function getHostSo() {
        return file_get_contents($this->hosts);
    }

    /**
     * <pre>
     * Retorna o arquivo de hosts original do Sistema Operacional
     * Caso ele não exista, o usuário é redirecionado para a tela de configuração do mesmo
     * </pre>
     * 
     * @return string
     */
    public function getHostsSoOriginal() {
        if (!file_exists($this->hostsOriginal)) {
            header("location: /vhost/original.php");
            exit;
        } else {
            return file_get_contents($this->hostsOriginal);
        }
    }

    /**
     * Cria um virtual host
     * 
     * @param string $params | Host a ser criado. Ex.: meusite.xs
     */
    public function hostCreate($params) {
        if (!$this->hostIsValid($params['host'])) {
            $_SESSION['msgs'][] = array(
                'msg' => "O virtual host informado já existe e não pode ser cadastrado.",
                'type' => "error"
            );

            header("location: /vhost/");
            exit;
        }

        //Pega o conteudo do arquivo
        $code = $this->getHostsSoOriginal();
        unset($code);

        //Adiciona o virtual host no jhosts
        $this->jhostAdd($params);

        //Atribui a $code o conteudo do arquivo de hosts do Sistema Operacional
        $code = $this->hostSoUpdate();

        //Adiciona o host no host do S.O
        $this->hostSoCreate($code);
        unset($code);

        //Atribui a $code o conteudo do arquivo de hosts virtuais do XAMPP
        $code = $this->hostsVirtualUpdate();

        //Cria o novo arquivo de Virtual Hosts do XAMPP
        $this->hostsVirtualCreate($code);
        unset($code);

        $_SESSION['msgs'][] = array(
            'msg' => "O virtual host foi adicionado, para utilizá-lo reinicialize o seu servidor Apache.",
            'type' => "success"
        );
    }

    /**
     * Edita um virtual host
     * 
     * @param string $params | Host a ser editado. Ex.: meusite.xs
     */
    public function hostEdit($params) {
        if (!$this->jhostIdExistis($params['id'])) {
            $_SESSION['msgs'][] = array(
                'msg' => "O virtual host informado não existe.",
                'type' => "error"
            );

            header("location: /vhost/");
            exit;
        } else {
            $this->hostRemove($params['id']);
            $this->hostCreate($params);
        }

        unset($_SESSION['msgs']);

        $_SESSION['msgs'][] = array(
            'msg' => "O virtual host foi editado, para utilizá-lo reinicialize o seu servidor Apache.",
            'type' => "success"
        );
    }

    /**
     * Verifica se um host é válido.
     * Para ser válido o host NÃO deve existir no jhost
     * 
     * @param type $host | Host a ser validado
     * @return boolean
     */
    private function hostIsValid($host) {
        if ($this->jhostExistis($host)) {
            return false;
        }

        return true;
    }

    /**
     * Renderiza a listagem dos hosts existentes em jhost
     * 
     * @return string|null
     */
    public function hostList() {
        $jhosts = $this->jhosts;

        if (count($jhosts) > 0) {

            asort($jhosts);

            $table = "<table>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th>Comandos</th>";
            $table .= "<th>Host</th>";
            $table .= "<th>Diretório</th>";
            $table .= "<th>Ip Virtual</th>";
            $table .= "<th>Ip S.O</th>";
            $table .= "</tr>";
            $table .= "</thead>";
            $table .= "<tbody>";
            foreach ($jhosts as $key => $value) {
                $table .= "<tr>";
                $table .= "
                    <td>
                        <a href='http://{$value->host}' title='Ir para {$value->host}'target='_blank'>Abrir</a>
                        <a class='linkEditarVirtualHost' data-id='{$key}' href='/vhost/index.php?id={$key}'>Editar</a>
                        <a class='linkRemoverVirtualHost' data-id='{$key}' href='/vhost/remover.php?id={$key}'>Remover</a>
                    </td>
                ";
                $table .= "<td><a href='http://{$value->host}' title='Ir para {$value->host}'target='_blank'>{$value->host}</a></td>";
                $table .= "<td>{$value->root}</td>";
                $table .= "<td>{$value->ip_virtual_host}</td>";
                $table .= "<td>{$value->ip_host_so}</td>";
                $table .= "</tr>";
            }
            $table .= "</tbody>";
            $table .= "</table>";

            return $table;
        } else {
            return null;
        }
    }

    /**
     * Cria uma copia do arquivo original de hosts do XAMPP para o arquivo httpd-vhosts_original.conf.
     * Em seguida cria um novo arquivo httpd-vhosts.conf
     */
    private function hostsVirtualOriginalBackup() {
        if (!file_exists($this->httpd . "_original.conf")) {
            copy($this->httpd_conf, $this->httpd . "_original.conf");
            unlink($this->httpd_conf);
            file_put_contents($this->httpd_conf, "NameVirtualHost 127.0.0.1\n\n");
        }
    }

    /**
     * Remove um virtual host
     * 
     * @param int $id | Id do host a ser removido - corresponde a posição do host no arquivo jhost
     */
    public function hostRemove($id) {

        //Verifica se o id de host informado é um id válido
        if (empty($this->jhosts[$id])) {
            $_SESSION['msgs'][] = array(
                'msg' => "O virtual host informado não existe!",
                'type' => "error"
            );

            header("location: " . $_SERVER['HTTP_REFERER']);
            exit;
        }

        //Remove o host do arquivo jhosts
        $this->jhostRemove($id);

        //Atribui a $code o conteudo do arquivo hosts do Sistema Operacional
        $code = $this->hostSoUpdate();

        //Cria o arquivo de hosts do Sistema Operacional
        $this->hostSoCreate($code);
        unset($code);

        //Atribui a $code o conteudo do arquivo de hosts do XAMPP
        $code = $this->hostsVirtualUpdate();

        //Cria o arquivo de hosts virtuais do XAMPP
        $this->hostsVirtualCreate($code);
        unset($code);

        $_SESSION['msgs'][] = array(
            'msg' => "O virtual host foi removido, reinicialize o seu servidor Apache.",
            'type' => "success"
        );
    }
    
    /**
     * Realiza o backup dos hosts do Sistema Operacional
     */
    private function hostSoBackup() {
        //Backup dos hosts do Sistema Operacional
        copy($this->hosts, $this->hosts . "_" . date("Y-m-d_H-i-s"));
    }

    /**
     * Cria o arquivo de host do Sistema Operacional
     * 
     * @param type $hosts | Conteudo do arquivo hosts
     */
    private function hostSoCreate($hosts) {
        $this->hostSoBackup();

        unlink($this->hosts);

        file_put_contents($this->hosts, $hosts);
    }

    /**
     * Cria um arquivo de hosts orignal
     * 
     * @param type $conteudo | Conteudo a ser inserido no arquivo de hosts original
     */
    public function hostSoOriginalCreate($conteudo) {
        unlink($this->hostsOriginal);

        file_put_contents($this->hostsOriginal, $conteudo);
        
        //Atribui a $code o conteudo do arquivo de hosts do Sistema Operacional
        $code = $this->hostSoUpdate();

        //Adiciona o host no host do S.O
        $this->hostSoCreate($code);
        unset($code);

        $_SESSION['msgs'][] = array(
            'msg' => "Arquivo de host original criado!",
            'type' => "success"
        );
    }

    /**
     * Verifica se o arquivo de hosts original existe
     * @return boolean
     */
    public function hostSoOriginalExists() {
        if (file_exists($this->hostsOriginal)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove um host do arquivo de hosts do Sistema Operacional
     */
    private function hostSoUpdate() {
        $code = $this->getHostsSoOriginal();
        
        foreach ($this->jhosts as $key => $value) {
            $code .= "\n{$value->ip_host_so}\t{$value->host}";
        }
        return $code;
    }

    /**
     * Realiza o backup dos hosts do arquivo httpd_vhosts.conf do XAMPP
     */
    private function hostsVirtualBackup() {
        copy($this->httpd_conf, $this->httpd . "_" . date("Y-m-d_H-i-s") . ".conf");
    }

    /**
     * Cria o arquivo de httpd_vhosts.conf do XAMPP
     * 
     * @param type $httpd | Conteudo do arquivo httpd_vhosts.conf do XAMPP
     */
    private function hostsVirtualCreate($httpd) {
        $this->hostsVirtualBackup();

        $this->hostsVirtualOriginalBackup();

        unlink($this->httpd_conf);
        file_put_contents($this->httpd_conf, $httpd);
    }

    /**
     * Atualiza o conteudo do arquivo de hosts virtuais do XAMPP
     * 
     * @return string | Retona o conteudo do arquivo de hosts virtuais do XAMPP
     */
    private function hostsVirtualUpdate() {
        $config = $this->getConfig();
        $code = "";

        $code .= "NameVirtualHost {$config->name_virtual_host}";

        $code .= "\n\n#localhost\n";
        $code .= "<VirtualHost {$config->name_virtual_host}>\n\t";
        $code .= "ServerName localhost\n\t";
        $code .= "DocumentRoot \"" . $config->localhost_dir . "\" \n\t";
        //permissoes de acesso
        $code .= "<Directory \"" . $config->localhost_dir . "\">\n\t\t";
        $code .= "Options Indexes FollowSymLinks \n\t\tAllowOverride All \n\t\tOrder allow,deny \n\t\tAllow From All \n\t";
        $code .= "</Directory>\n";
        $code .= "</VirtualHost>";


        foreach ($this->jhosts as $key => $value) {
            //Virtual Host
            $code .= "\n\n#" . $value->host . "\n";
//            $code .= "<VirtualHost {$value->ip_virtual_host}>\n\t";
            $code .= "<VirtualHost {$config->name_virtual_host}>\n\t";
            $code .= "ServerName " . $value->host . "\n\t";
            $code .= "DocumentRoot \"" . $value->root . "\" \n\t";
            
            foreach ($value->variables as $key => $val) {
                $code .= "SetEnv " . $key . " \"" . $val . "\" \n\t";
            }
            
            //permissoes de acesso
            $code .= "<Directory \"" . $value->root . "\">\n\t\t";
            $code .= "Options Indexes FollowSymLinks \n\t\tAllowOverride All \n\t\tOrder allow,deny \n\t\tAllow From All \n\t";
            $code .= "</Directory>\n";
            $code .= "</VirtualHost>";
        }

        return $code;
    }

    /**
     * Adiciona um virtual host no arquivo jhosts
     * 
     * @param type $params
     */
    private function jhostAdd($params) {
        $params['root'] = preg_replace("/(\/)+$/", "", $params['root']);
        $this->jhostBackup();
        
        //Atualiza vhosts.json
        $jhosts = $this->jhosts;
        $jhosts[] = array(
//            "ip_virtual_host" => $params['ip_virtual_host'],
            "ip_virtual_host" => $this->getConfig()->name_virtual_host,
            "ip_host_so" => $params['ip_host_so'],
            "host" => $params['host'],
            "root" => $params['root'],
            "variables" => $params['variables']
        );
        $jhosts = json_encode($jhosts);

        unlink($this->jfile);
        file_put_contents($this->jfile, $jhosts);

        $this->jhosts = json_decode(file_get_contents($this->jfile));
    }

    /**
     * Realiza o backup do arquivo jhosts
     * 
     */
    private function jhostBackup() {
        copy($this->jfile, "jhosts_" . date("Y-m-d_H-i-s"));
    }

    /**
     * Verifica se um host existe no arquivo jhosts
     * 
     * @param type $host | Host a ser encontrado
     * @return boolean
     */
    private function jhostExistis($host) {
        $host = strtolower($host);
        if (count($this->jhosts) > 0) {
            foreach ($this->jhosts as $key => $value) {
                if ($value->host == $host) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * Verifica se um id de host existe no arquivo jhosts
     * 
     * @param type $id | ID do host a ser encontrado
     * @return boolean
     */
    private function jhostIdExistis($id) {
        if (count($this->jhosts) > 0) {
            foreach ($this->jhosts as $key => $value) {
                if ($key == $id) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * Remove um host do arquivo  jhosts
     * 
     * @param int $id | Id do virtual host a ser removido - corresponde a posição do virtual host no arquivo jhosts
     */
    private function jhostRemove($id) {

        //Realiza o backup do arquivo jhosts
        $this->jhostBackup();

        $jhost = array();

        foreach ($this->jhosts as $key => $value) {
            if ($key != $id) {
                $jhost[] = $value;
            }
        }

        unlink($this->jfile);

        file_put_contents($this->jfile, json_encode($jhost));

        //Atualiza a lista de hosts virtuais
        $this->jhosts = json_decode(file_get_contents($this->jfile));
    }
}

?>
