<?php
ob_start();
session_start();
require_once 'Vhost.php';

$class = new Vhost();
$class->getConfig();
$class->getHostsSoOriginal();
$values = $class->getConfig();

if (isset($_GET['id'])) {
    $values = $class->getHostById($_GET['id']);
    $values->ip_host_so = $values->ip_host_so;
} else {
    $values->ip_host_so = $values->ip_virtual_host;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="vhost.css" rel="stylesheet" />
        <script src="jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="vhost.js" type="text/javascript"></script>
        <title>XAMPP - Virtual Hosts - v-1.0.0</title>
    </head>
    <body>
        <?php
        require_once 'menu.php';
        ?>
        <form method="post" action="/vhost/action.php">
            <input type="hidden" name="form" id="form" value="<?php echo (!isset($_GET['id'])) ? "createVirtualHost" : "editVirtualHost"; ?>"/>
            <input type="hidden" name="id" id="form" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : ""; ?>"/>
<!--            <p>
                <label for="ip_virtual_host">IP Virtual Host</label><br>
                <input type="text" name="ip_virtual_host" id="ip_virtual_host" placeholder="Ex.: 127.0.0.1, *:80" value="<?php echo $values->ip_virtual_host; ?>"/>
            </p>-->
            <p>
                <label for="ip_host_so">IP Host S.O</label><br>
                <input type="text" name="ip_host_so" id="ip_host_so" placeholder="Ex.: 127.0.0.1, *:80" value="<?php echo $values->ip_host_so; ?>"/>
            </p>
            <p>
                <label for="host">Nome do host desejado</label><br>
                <input type="text" name="host" id="host" placeholder="Ex: meusite.dev" value="<?php echo $values->host; ?>"/>
            </p>
            <p>
                <label for="root">Diretório</label><br>
                <input type="text" name="root" id="diretorio" placeholder="Ex: /Users/usuario/Documents/site" value="<?php echo $values->root; ?>" />
            </p>
            <label for="variables">Variáveis de Ambiente</label><br>
            <section id="variables">
                <?php
                $variables = json_decode(json_encode($values->variables), 1);

                if (!empty($variables)) {
                    $variables_key = array_keys($variables);
                    $variables_val = array_values($variables);

                    for ($i = 0, $iMax = count($variables); $i < $iMax; $i++) {
                        ?>
                        <section class="variable">
                            <input type="text" name="variables_key[]" class="variables_key" placeholder="Variável" value="<?php echo $variables_key[$i]; ?>" data-index="<?php echo ($i + 1); ?>" />
                            <input type="text" name="variables_val[]" class="variables_val" placeholder="Valor" value="<?php echo $variables_val[$i]; ?>" data-index="<?php echo ($i + 1); ?>" />
                        </section>
                        <?php
                    }
                } else {
                    ?>
                    <section class="variable">
                        <input type="text" name="variables_key[]" class="variables_key" placeholder="Variável" data-index="1" />
                        <input type="text" name="variables_val[]" class="variables_val" placeholder="Valor" data-index="1" />
                    </section>
                    <?php
                }
                ?>
            </section>
            <input type="button" id="addVariable" value="Add" />
            <input type="button" id="delVariable" value="Del" />
            <p>
                <input type="submit" value="Criar Virtual Host" />
            </p>
        </form>
        <?php
        echo $class->hostList();
        ?>
    </body>
</html>
