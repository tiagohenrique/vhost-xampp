<?php
ob_start();
session_start();
require_once 'Vhost.php';

$class = new Vhost();
$class->getConfig();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="vhost.css" rel="stylesheet" />
        <script src="jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="vhost.js" type="text/javascript"></script>
        <title>XAMPP - Virtual Hosts</title>
    </head>
    <body>
        <?php
        require_once 'menu.php';
        ?>
        <form method="post" action="/vhost/action.php">
            <input type="hidden" name="form" id="form" value="createHostOriginal"/>
            <p>
                <label for="original">Conteudo do arquivo hosts original</label><br>
                <textarea name="original" id="original" placeholder="Insira o conteudo do arquivo original de hosts do seu Sistema operacional"></textarea>
            </p>
            <p>
                <input type="submit" value="Salvar" />
            </p>
        </form>
        <table id="tbHostsList">
            <tr>
                <?php
                if ($class->hostSoOriginalExists()) {
                    $so = $class->getHostsSoOriginal();
                    echo "<td>";
                    echo "<div class='title'>Conteudo do arquivo de host original:</div>";
                    echo "<pre>";
                    print_r($so);
                    echo "</pre>";
                    echo "</td>";
                }

                $so = $class->getHostSo();
                echo "<td>";
                echo "<div class='title'>Conteudo do arquivo de host atual:</div>";
                echo "<pre>";
                print_r($so);
                echo "</pre>";
                echo "</td>";
                ?>
            </tr>
        </table>
    </body>
</html>
