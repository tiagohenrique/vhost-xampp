<?php
    ob_start();
    session_start();
    require_once 'Vhost.php';
    $class = new Vhost(false);
    
    if ($class->configExistis()) {
        $values = $class->getConfig();
    }
    else {
        $values = array();
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="vhost.css" rel="stylesheet" />
        <script src="jquery-2.0.3.min.js" type="text/javascript"></script>
        <script src="vhost.js" type="text/javascript"></script>
        <title>XAMPP - Virtual Hosts</title>
    </head>
    <body>
        <?php
        require_once 'menu.php';
        ?>
        <form method="post" action="/vhost/action.php">
            <input type="hidden" name="form" id="form" value="createConfig"/>
            <p>
                <label for="name_virtual_host">Name Virtual Host</label><br>
                <input type="text" name="name_virtual_host" id="name_virtual_host" placeholder="Ex.: 127.0.0.1, *:80" value="<?php echo $values->name_virtual_host; ?>"/>
            </p>
<!--            <p>
                <label for="ip_virtual_host">IP Virtual Host</label><br>
                <input type="text" name="ip_virtual_host" id="ip_virtual_host" placeholder="Ex.: 127.0.0.1, *:80" value="<?php echo $values->ip_virtual_host; ?>"/>
            </p>-->
            <p>
                <label for="dir_hosts_so">Diretório do hosts</label><br>
                <input type="text" name="dir_hosts_so" id="dir_hosts_so" placeholder="Diretório do arquivo hosts do seu sistema operacional" value="<?php echo $values->dir_hosts_so; ?>"/>
            </p>
            <p>
                <label for="dir_httpd">Diretório do httpd-vhosts.conf</label><br>
                <input type="text" name="dir_httpd" id="dir_httpd" placeholder="Diretório do arquivo httpd-vhosts.conf do XAMPP" value="<?php echo $values->dir_httpd; ?>" />
            </p>
            <p>
                <label for="localhost_dir">Diretório do localhost</label><br>
                <input type="text" name="localhost_dir" id="localhost_dir" placeholder="Diretório para acesso ao localhost - htdocs, www" value="<?php echo $values->localhost_dir; ?>" />
            </p>
            <p>
                <input type="submit" value="Salvar" />
            </p>
        </form>
        <?php
            echo $class->configList();
        ?>
    </body>
</html>
