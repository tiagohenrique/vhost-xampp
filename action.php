<?php

ob_start();
session_start();
require_once 'Vhost.php';

if (isset($_POST['form'])) {

    switch ($_POST['form']) {
        //Cria um virtual host
        case 'createVirtualHost':
            $params = array();

            if ((isset($_POST['host'])) && (trim($_POST['host']) != "")) {
                $params['host'] = trim($_POST['host']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O host informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }

            if ((isset($_POST['root'])) && (trim($_POST['root']) != "")) {
                $params['root'] = trim($_POST['root']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O diretório informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }

//            if ((isset($_POST['ip_virtual_host'])) && (trim($_POST['ip_virtual_host']) != "")) {
//                $params['ip_virtual_host'] = trim($_POST['ip_virtual_host']);
//            } else {
//                $_SESSION['msgs'][] = array(
//                    'msg' => "O IP Virtual Host informado é inválido!",
//                    'type' => "error"
//                );
//
//                header("location: /vhost/");
//                exit;
//            }

            if ((isset($_POST['ip_host_so'])) && (trim($_POST['ip_host_so']) != "")) {
                $params['ip_host_so'] = trim($_POST['ip_host_so']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O IP Host S.O informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }
            
            $params['variables'] = array();
            
            if ((isset($_POST['variables_key'])) && (isset($_POST['variables_val']))) {
                $keys = $_POST['variables_key'];
                $values = $_POST['variables_val'];
                
                for ($i = 0, $iMax = count($keys); $i < $iMax; $i++) {
                    if ((!empty($keys[$i])) && (!empty($values[$i]))) {
                        $params['variables'][$keys[$i]] = $values[$i];
                    }
                }
            }

            $class = new Vhost();
            $class->hostCreate($params);

            header("location: /vhost/");

            break;

        case 'editVirtualHost':
            if ((isset($_POST['host'])) && (trim($_POST['host']) != "")) {
                $params['host'] = trim($_POST['host']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O host informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }

            if ((isset($_POST['root'])) && (trim($_POST['root']) != "")) {
                $params['root'] = trim($_POST['root']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O diretório informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }

//            if ((isset($_POST['ip_virtual_host'])) && (trim($_POST['ip_virtual_host']) != "")) {
//                $params['ip_virtual_host'] = trim($_POST['ip_virtual_host']);
//            } else {
//                $_SESSION['msgs'][] = array(
//                    'msg' => "O IP Virtual Host informado é inválido!",
//                    'type' => "error"
//                );
//
//                header("location: /vhost/");
//                exit;
//            }

            if ((isset($_POST['ip_host_so'])) && (trim($_POST['ip_host_so']) != "")) {
                $params['ip_host_so'] = trim($_POST['ip_host_so']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O IP Host S.O informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }

            if (isset($_POST['id'])) {
                $params['id'] = $_POST['id'];
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O Host informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/");
                exit;
            }
            
            $params['variables'] = array();
            
            if ((isset($_POST['variables_key'])) && (isset($_POST['variables_val']))) {
                $keys = $_POST['variables_key'];
                $values = $_POST['variables_val'];
                
                for ($i = 0, $iMax = count($keys); $i < $iMax; $i++) {
                    if ((!empty($keys[$i])) && (!empty($values[$i]))) {
                        $params['variables'][$keys[$i]] = $values[$i];
                    }
                }
            }
            
            $class = new Vhost();
            $class->hostEdit($params);

            header("location: /vhost/");
            break;

        //Cria o bakcup do arquivo de hosts original do Sistema Operacional
        case 'createHostOriginal':
            if ((isset($_POST['original'])) && (trim($_POST['original']) != "")) {
                $conteudo['conteudo'] = trim($_POST['original']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O conteudo informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/original.php");
                exit;
            }

            $class = new Vhost();
            $class->hostSoOriginalCreate($conteudo);

            header("location: " . $_SERVER['HTTP_REFERER']);

            break;
        case 'createConfig':
            $params = array();

            if ((isset($_POST['dir_hosts_so'])) && (trim($_POST['dir_hosts_so']) != "")) {
                $params['dir_hosts_so'] = trim($_POST['dir_hosts_so']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O diretório do arquivo hosts informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/config.php");
                exit;
            }

            if ((isset($_POST['dir_httpd'])) && (trim($_POST['dir_httpd']) != "")) {
                $params['dir_httpd'] = trim($_POST['dir_httpd']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O diretório do arquivo httpd-vhosts.conf informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/config.php");
                exit;
            }

            if ((isset($_POST['name_virtual_host'])) && (trim($_POST['name_virtual_host']) != "")) {
                $params['name_virtual_host'] = trim($_POST['name_virtual_host']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O nome do virtual host informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/config.php");
                exit;
            }

//            if ((isset($_POST['ip_virtual_host'])) && (trim($_POST['ip_virtual_host']) != "")) {
//                    $params['ip_virtual_host'] = trim($_POST['ip_virtual_host']);
//            } else {
//                $_SESSION['msgs'][] = array(
//                    'msg' => "O IP do virtual host informado é inválido!",
//                    'type' => "error"
//                );
//
//                header("location: /vhost/config.php");
//                exit;
//            }

            if ((isset($_POST['localhost_dir'])) && (trim($_POST['localhost_dir']) != "")) {
                $params['localhost_dir'] = trim($_POST['localhost_dir']);
            } else {
                $_SESSION['msgs'][] = array(
                    'msg' => "O diretorio de acesso ao localhost informado é inválido!",
                    'type' => "error"
                );

                header("location: /vhost/config.php");
                exit;
            }

            $class = new Vhost(false);
            $class->setConfig($params);

            $_SESSION['msgs'][] = array(
                'msg' => "Configurações salvas!",
                'type' => "success"
            );

            header("location: /vhost/");
            exit;

            break;
        default:

            $_SESSION['msgs'][] = array(
                'msg' => "Formulário de entrada desconhecido!",
                'type' => "error"
            );

            header("location: /vhost/");
            exit;

            break;
    }
}
?>