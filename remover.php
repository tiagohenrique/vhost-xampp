<?php
    ob_start();
    session_start();
    require_once 'Vhost.php';
    
    $class = new Vhost();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="vhost.css" rel="stylesheet" />
        <title>XAMPP - Virtual Hosts</title>
    </head>
    <body>
        <?php
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];

            $vhost = new Vhost();
            $vhost->hostRemove($id);
            
            header("location: " . $_SERVER['HTTP_REFERER']);
        }
        ?>
    </body>
</html>
