$(document).ready(function() {
    removeVirtualHost.init();
    addVE.init();
});

var removeVirtualHost = {
    init: function() {
        $(".linkRemoverVirtualHost").on("click", function(e) {
            if (!confirm("Deseja remover este virtual host?")) {
                e.preventDefault();
            }
        });
    }
};

var addVE = {
    init: function() {

        $("#addVariable").on("click", function() {
            addVE.create();
        });

        $("#delVariable").on("click", function() {
            addVE.remove();
        });
    },
    create: function() {
        var all_keys = $(".variables_key");
        var all_val = $(".variables_val");
        var last_key = $(all_keys).last();
        var index;
        var variable_key;
        var variable_val;
        var variable_clone = $(".variable").last().clone();
        
        if ($(all_keys).length > 0) {
            for (i = 0; i < $(all_keys).length; i++) {
                variable_key = all_keys[i];
                variable_val = all_val[i];
                
                if (($(variable_key).val() === "") || ($(variable_val).val() === "")) {
                    return;
                }
            }
        }
        
        if ($(last_key).length) {
            index = $(last_key).data("index") + 1;
        }
        else {
            index = 1;
        }
        
        $(variable_clone).find(".variables_key").attr("data-index", index).val("");
        $(variable_clone).find(".variables_val").attr("data-index", index).val("");
        
        $("#variables").append($(variable_clone));
    },
    remove: function() {
        var all_keys = $(".variables_key");
        var all_val = $(".variables_val");
        var variable_key;
        var variable_val;
        
        if ($(all_keys).length > 1) {
            for (i = 0; i < $(all_keys).length; i++) {
                variable_key = all_keys[i];
                variable_val = all_val[i];
                
                if (($(variable_key).val() === "") && ($(variable_val).val() === "")) {
                    $(variable_key).parent().remove();
                    return;
                }
            }
        }
    }
};