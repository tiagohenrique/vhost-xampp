<div id="menu">
    <a href="/vhost/" title="Criar um virtual host">Virtual Host</a>
    <a href="/vhost/original.php" title="Atualizar arquivo de hosts original">Host S.O</a>
    <a href="/vhost/config.php" title="Configurações">Configurações</a>
</div>
<?php
if (count($_SESSION['msgs']) > 0) {
    foreach ($_SESSION['msgs'] as $key => $msg) {
        echo "<span class='{$msg['type']}'>{$msg['msg']}</span>";
    }
    unset($_SESSION['msgs']);
}
?>